package com.mastercard.cas.ds.test.domain.calls;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.restassured.response.Response;

@Component
public class DSApiCall extends BaseApiCall{
	 @Value(("${base.url.areq}"))
	    private String dsAreqUrl;
	 
	 public Response doPost(String url, String payload) {
			Response response = restClient.post(dsAreqUrl+url, payload);
			contextObject.setHttpStatusCode(String.valueOf(response.getStatusCode()));
			return response;
		}

}
