package com.mastercard.cas.ds.test.constants;

public class JsonNames {

	public static final String MESSAGECATEGORY = "messageCategory";
	public static final String THREEDSREQUESTORID = "threeDSRequestorID";
	public static final String ECI = "eci";
	public static final String MESSAGEVERSION = "messageVersion";
	public static final String TRANSSTATUS = "transStatus";
	public static final String MESSAGETYPE = "messageType";
	public static final String THREEDSSRVRTRANID = "threeDSServerTransID";
	public static final String THREEDSSERVERTRANSID = "threeDSServerTransID";
	public static final String DS_TransID = "dsTransID";
	public static final String ACCT_NUMBER = "acctNumber";
	public static final String ACS_TRANS_ID = "acsTransID";
	public static final String ACS_OPERATOR_ID = "acsOperatorID";
	public static final String AUTH_VALUE = "authenticationValue";
	public static final String DS_REF_NUMBER = "dsReferenceNumber";
	
	public static final String ERROR_CODE = "errorCode";
	public static final String ERROR_DESCRIPTION = "errorDescription";
	public static final String ERROR_DETAIL = "errorDetail";
	public static final String ERROR_MSG_TYPE = "errorMessageType";
	public static final String ERROR_COMPONENT = "errorComponent";
	public static final String MESSAGE_TYPE = "messageType";
	public static final String DS_TRANS_ID = "dsTransID";

}
