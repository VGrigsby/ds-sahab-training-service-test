package com.mastercard.cas.ds.test.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.mastercard.quality.engineering.mtaf.jbehave.configuration.spring.MTAFJBehaveToolsConfiguration;

@Configuration
@ComponentScan(basePackages = {"com.mastercard.cas.ds,com.mastercard.ess.common.functions"})
@Import({MTAFJBehaveToolsConfiguration.class})
@PropertySources({
   @PropertySource(value="config/${environment}/application.properties"),
})
public class ApiApplicationConfig {
}
