package com.mastercard.cas.ds.test.domain.calls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.test.context.ContextObject;

import io.restassured.response.Response;

@Component
public class BaseApiCall {
	@Autowired
	ContextObject contextObject;

	@Autowired
	protected RestClient restClient;

	public Response doPost(String url, String payload) {
		Response response = restClient.post(url, payload);
		contextObject.setHttpStatusCode(String.valueOf(response.getStatusCode()));
		return response;
	}

}
