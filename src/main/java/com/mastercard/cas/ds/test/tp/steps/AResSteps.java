package com.mastercard.cas.ds.test.tp.steps;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.test.constants.JsonNames;
import com.mastercard.cas.ds.test.constants.MessageType;
import com.mastercard.cas.ds.test.step.BaseStep;

@Component("AcsToDsAResSteps")
public class AResSteps extends BaseStep {

	@Then("ACS sends DS Authentication Response with transaction status as '$transStatus' and eci value")
	@Alias("DS2.0 sends ARES with transaction status as '$transStatus' and eci value")
	public void thenAcsSendsDSAuthenticationResponseWithTransactionStatusAndEciValue(
			@Named("transStatus") String transStatus) {
		authenticationResponseVerification(transStatus);
		if (containsField(MessageType.ARES, JsonNames.ECI)) {
			verifyField(MessageType.ARES, JsonNames.ECI, "01", "ECI value didn't match");
		}
	}

	@Then("DS send Error Response for missing account number")
	public void thenDSSendsMissingActErrorResponse() {
		verifyField(MessageType.ARES, JsonNames.ERROR_CODE, "201", "Error code didn't match");
		verifyField(MessageType.ARES, JsonNames.ERROR_DESCRIPTION, "Required element missing",
				"Error description didn't match");
		verifyField(MessageType.ARES, JsonNames.ERROR_DETAIL, "acctNumber", "Error Details didn't match");
		verifyField(MessageType.ARES, JsonNames.ERROR_MSG_TYPE, "AReq", "Error Msg Type didn't match");
		verifyField(MessageType.ARES, JsonNames.ERROR_COMPONENT, "D", "Error Component didn't match");
		verifyField(MessageType.ARES, JsonNames.MESSAGE_TYPE, "Erro", "Message type didn't match");

	}
	
	
	@Then("DS send Error Response for invalid 3DS reference number")
	public void thenDSSendsInvalidRefNumberErrorResponse() {
		verifyField(MessageType.ARES, JsonNames.ERROR_CODE, "303", "Error code didn't match");
		verifyField(MessageType.ARES, JsonNames.ERROR_DESCRIPTION, "Access denied, invalid endpoint",
				"Error description didn't match");
		verifyField(MessageType.ARES, JsonNames.ERROR_DETAIL, "threeDSServerRefNumber not recognized");
		verifyField(MessageType.ARES, JsonNames.ERROR_MSG_TYPE, "AReq", "Error Msg Type didn't match");
		verifyField(MessageType.ARES, JsonNames.ERROR_COMPONENT, "D", "Error Component didn't match");
		verifyField(MessageType.ARES, JsonNames.MESSAGE_TYPE, "Erro", "Message type didn't match");

	}

	private void authenticationResponseVerification(String expectedTranStatus) {
		verifyField(MessageType.ARES, JsonNames.TRANSSTATUS, expectedTranStatus, "Transaction status didn't match");

		verifyField(MessageType.ARES, JsonNames.MESSAGEVERSION, "2.1.0", "Message version didn't match");

		verifyField(MessageType.ARES, JsonNames.MESSAGETYPE, "ARes", "Message Type didn't match");
		verifyField(MessageType.ARES, JsonNames.THREEDSSERVERTRANSID, "3333eedc-3671-4595-8b7f-f06264c9e08a",
				"ThreeDsServer Trans ID didn't match");
		verifyField(MessageType.ARES, JsonNames.ACCT_NUMBER, "5472000000005001", "Acct Number didn't match");
		verifyField(MessageType.ARES, JsonNames.ACS_TRANS_ID, "1111eedc-3671-4595-8b7f-f06264c9e08b",
				"Acs TransID didn't match");
		verifyField(MessageType.ARES, JsonNames.ACS_OPERATOR_ID, "ACS_RSA_020100_00011", "acsOperatorID didn't match");
		verifyField(MessageType.ARES, JsonNames.AUTH_VALUE, "01", "authenticationValue didn't match");
		verifyField(MessageType.ARES, JsonNames.DS_REF_NUMBER, "DS_MAST_020100_00038",
				"dsReferenceNumber didn't match");
		verifyField(MessageType.ARES, JsonNames.DS_TransID, "2222eedc-3671-4595-8b7f-f06264c9e08a",
				"dsTransID didn't match");
		verifyField(MessageType.ARES, JsonNames.TRANSSTATUS, "Y", "transStatus didn't match");

	}
}