package com.mastercard.cas.ds.test.tp.steps;


import org.apache.commons.lang3.StringUtils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.When;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.test.config.DataDomainLoader;
import com.mastercard.cas.ds.test.constants.JsonNames;
import com.mastercard.cas.ds.test.constants.MessageType;
import com.mastercard.cas.ds.test.step.BaseStep;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AreqSteps extends BaseStep {
	
	@Autowired
	private DataDomainLoader domainLoader;

	@Given("ThreeDsServer sends a AReq message '$message' to Directory Server (DS)")
	public void givenThreeDsServerSendsAAreqMessageToDirectoryServer(@Named("message") String message)
			throws Exception {
		log.info("givenThreeDsServerSendsAAreqMessageToDirectoryServer");
		JSONObject request = domainLoader.buildAReqMessage(message);

		if (!message.contains(JsonNames.MESSAGECATEGORY) && StringUtils.isNotEmpty(System.getProperty("test.message.category.value"))) {
			request.put(JsonNames.MESSAGECATEGORY, System.getProperty("test.message.category.value"));
			log.info("replaced: 'messageCategory' with value: '" + System.getProperty("test.message.category.value") + "'");
		}

		if (!message.contains(JsonNames.THREEDSREQUESTORID)
				&& (System.getProperty("test.threeds.requestor.id") != null)) {
			request.put(JsonNames.THREEDSREQUESTORID, System.getProperty("test.threeds.requestor.id"));
			log.info("replaced: 'threeDSRequestorID' with value: '" + System.getProperty("test.threeds.requestor.id")
					+ "'");
		}

		log.info("AReq received from ThreeDsServer: " + request);
		putInContext(MessageType.AREQ, request);
	}

	@When("DS process the AReq message")
	public void whenTheAReqMessageIsProcessedByDs() {
		JSONObject request  = (JSONObject) getFromContext(MessageType.AREQ);
		executeAReqAPICall(request.toString(), "3ds/processAReq");
		
	}

	
	
}
