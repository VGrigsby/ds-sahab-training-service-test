package com.mastercard.cas.ds.test.config;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mastercard.ess.common.functions.FunctionParser;
import com.mastercard.quality.engineering.mtaf.jbehave.context.DomainLoader;

@Component
public class DataDomainLoader extends DomainLoader {

	@Autowired
	private FunctionParser functionParser;



	public JSONObject buildAReqMessage(String message) {
		JSONObject parsedMessage = functionParser.parse(message);
		return parsedMessage;
	}

}