package com.mastercard.cas.ds.test.config;

import java.lang.reflect.Type;

import org.apache.commons.lang3.StringUtils;
import org.jbehave.core.steps.ParameterConverters.ParameterConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

public class PropertyParamConverter implements ParameterConverter {
	
	private final static Logger LOG = LoggerFactory.getLogger(PropertyParamConverter.class);

	private String propertyIndicatorPrefix;
	protected Environment properties;

	public PropertyParamConverter(Environment properties) {
		this.propertyIndicatorPrefix = properties.getProperty("jbehave.converters.property_indicator_prefix", "p:")
				.trim();
		this.properties = properties;
	}

	@Override
	public boolean accept(Type type) {
		if (type instanceof Class<?>) {
			return String.class.isAssignableFrom((Class<?>) type);
		}
		return false;
	}

	@Override
	public Object convertValue(String value, Type type) {
		if (StringUtils.isBlank(value) || !value.startsWith(propertyIndicatorPrefix)) {
			return value;
		}
		String convertedValue = properties.getProperty(value.substring(propertyIndicatorPrefix.length()));

		LOG.info("Parameter converted:[" + value + "] >> [" + convertedValue + "]");

		return convertedValue;
	}
}
