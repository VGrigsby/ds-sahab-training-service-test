package com.mastercard.cas.ds.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.ParameterConverter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mastercard.cas.ds.test.config.ApiApplicationConfig;
import com.mastercard.cas.ds.test.config.PropertyParamConverter;
import com.mastercard.quality.engineering.mtaf.jbehave.configuration.MastercardJBehaveStories;

public class ApiRegressionStories extends MastercardJBehaveStories {
    @Override
    public List<String> storyPaths() {
    	List<String> paths = null;
        String storyName = System.getProperty("storyName");
        if (StringUtils.isNotEmpty(storyName)) {
            paths = new StoryFinder().findPaths(
                    CodeLocations.codeLocationFromClass(this.getClass()),
                    "**/stories/Regression/" + storyName, "");
        } else {
            paths = new StoryFinder().findPaths(
                    CodeLocations.codeLocationFromClass(this.getClass()),
                    "**/*.story", "");
        }
        return paths;
    }
    @Override
	public ApplicationContext getAnnotatedApplicationContext() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.getEnvironment().setActiveProfiles(System.getProperty("environment"));
		context.register(ApiApplicationConfig.class);
		context.refresh();
		return context;    
	}

	@Override
	public Configuration configuration() {
		ParameterConverters converters = super.configuration().parameterConverters();
		List<ParameterConverter> customConverters = new ArrayList<ParameterConverter>();
		customConverters.add(new PropertyParamConverter(getAnnotatedApplicationContext().getEnvironment()));
		converters.addConverters(customConverters);
		return super.configuration().useStoryControls(new StoryControls().doSkipScenariosAfterFailure(false)).useParameterConverters(converters);
	}
}