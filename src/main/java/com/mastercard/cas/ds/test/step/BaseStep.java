package com.mastercard.cas.ds.test.step;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.mastercard.cas.ds.test.constants.MessageType;
import com.mastercard.cas.ds.test.context.ContextObject;
import com.mastercard.cas.ds.test.domain.calls.DSApiCall;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public abstract class BaseStep {
	@Autowired
	private ContextObject context;

	@Autowired
	private DSApiCall dsApiCall;

	protected void putInContext(MessageType messageType, JSONObject request) {
		context.putObject(messageType, request);

	}

	protected Object getFromContext(MessageType areq) {
		return context.getObject(areq);
	}

	protected void executeAReqAPICall(String request, String url) {
		Response resp = dsApiCall.doPost(url, request);
		context.putObject(MessageType.ARES, resp);
	}

	protected void executeAReqAPICall(JSONObject request, String url) {
		Response resp = dsApiCall.doPost(url, request.toString());
		context.putObject(MessageType.ARES, resp);
	}

	protected void verifyResponseCode(MessageType messageType, int responseCode) {
		Object obj = getFromContext(messageType);
		assertNotNull("No Valid Response found in context.", obj);
		Response resp = (Response) obj;
		int statusCode = resp.getStatusCode();
		assertEquals(responseCode, statusCode);

	}

	protected void verifyErrorField(MessageType messageType, String field, String expectedValue) {
		Object obj = getFromContext(messageType);
		assertNotNull("No Valid Response found in context.", obj);
		Response resp = (Response) obj;
		resp.then().statusCode(200);
		JsonPath jsonPathEvaluator = resp.jsonPath();
		assertEquals(expectedValue, jsonPathEvaluator.get(field));

	}

	protected void verifyField(MessageType messageType, String field, String expectedValue) {
		Object obj = getFromContext(messageType);
		assertNotNull("No Valid Response found in context.", obj);
		Response resp = (Response) obj;
		resp.then().statusCode(200);
		JsonPath jsonPathEvaluator = resp.jsonPath();
		assertEquals(expectedValue, jsonPathEvaluator.get(field));

	}

	protected void verifyField(MessageType messageType, String field, String expectedValue, String errorMessage) {
		Object obj = getFromContext(messageType);
		assertNotNull("No Valid Response found in context.", obj);
		Response resp = (Response) obj;
		resp.then().statusCode(200);
		JsonPath jsonPathEvaluator = resp.jsonPath();
		assertEquals(errorMessage, expectedValue, jsonPathEvaluator.get(field));

	}
	
	protected boolean containsField(MessageType messageType, String fieldName) {
		Object obj = getFromContext(messageType);
		assertNotNull("No Valid Response found in context.", obj);
		Response resp = (Response) obj;
		return StringUtils.isNoneBlank(resp.jsonPath().getString(fieldName));
	}

}
