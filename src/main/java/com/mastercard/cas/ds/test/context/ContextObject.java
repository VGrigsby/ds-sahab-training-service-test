package com.mastercard.cas.ds.test.context;

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.test.constants.MessageType;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class ContextObject {

	private String httpStatusCode;

	private Map<MessageType, Object> objects = new HashedMap<>();

	public void putObject(MessageType msgType, Object obj) {
		objects.put(msgType, obj);
	}

	public Object getObject(MessageType msgType) {
		return objects.get(msgType);
	}

}
