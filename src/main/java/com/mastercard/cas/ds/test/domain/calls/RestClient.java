package com.mastercard.cas.ds.test.domain.calls;

import org.springframework.stereotype.Component;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RestClient {
	
	
	public Response post(String url, String payload) {
		RequestSpecification request = RestAssured.given();
		request.contentType(ContentType.JSON).body(payload);

		Response response = request.accept(ContentType.JSON).post(url);
		log.info("Status Code: {}", response.getStatusCode());
		return response;
	}

}
