Narrative:
As an user
I want to test 3d reference number validation response from ACS for AREQ message version 2.1.0

Meta:
@storyType API
@readyForRunOnDev yes
@readyForRunOnStage yes
@AReqPositiveFlow yes
@PaymentFlows yes
@version 2.1.0

Scenario: Sxxxxxx - DS should send validation error if 3DS reference number is invalid in Areq
Given ThreeDsServer sends a AReq message 'message' to Directory Server (DS)
When DS process the AReq message
Then DS send Error Response for invalid 3DS reference number
Examples:
|Meta:				|message																																			|
|@TestId TCxxxxx	|loadTemplate(areq/AREQ).replace(deviceChannel,01;threeDSServerTransID,U:uuid;sdkTransID,U:uuid;threeDSServerRefNumber,abc)		