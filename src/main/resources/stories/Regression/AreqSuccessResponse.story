Narrative:
As an user
I want to test successful response from ACS for AREQ message version 2.2.0

Meta:
@storyType API
@readyForRunOnDev yes
@readyForRunOnStage yes
@AReqPositiveFlow yes
@PaymentFlows yes
@version 2.1.0

Scenario: Sxxxxxx - DS should send transStatus Y to 3DS when it receives AREQ
Given ThreeDsServer sends a AReq message 'message' to Directory Server (DS)
When DS process the AReq message
Then ACS sends DS Authentication Response with transaction status as 'Y' and eci value
Examples:
|Meta:				|message																																			|
|@TestId TCxxxxx	|loadTemplate(areq/AREQ).replace(deviceChannel,01;threeDSServerTransID,U:uuid;sdkTransID,U:uuid)		