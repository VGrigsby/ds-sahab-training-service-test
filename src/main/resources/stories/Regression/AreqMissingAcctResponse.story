Narrative:
As an user
I want to test missing account number validation response from ACS for AREQ message version 2.1.0

Meta:
@storyType API
@readyForRunOnDev yes
@readyForRunOnStage yes
@AReqPositiveFlow yes
@PaymentFlows yes
@version 2.1.0

Scenario: Sxxxxxx - DS should send validation error if account number is missing in Areq
Given ThreeDsServer sends a AReq message 'message' to Directory Server (DS)
When DS process the AReq message
Then DS send Error Response for missing account number
Examples:
|Meta:				|message																																			|
|@TestId TCxxxxx	|loadTemplate(areq/AREQ).replace(deviceChannel,01;threeDSServerTransID,U:uuid;sdkTransID,U:uuid).remove(acctNumber,)		